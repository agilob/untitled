#!/bin/sh
set -e

echo "nameserver 8.8.8.8" >> /etc/resolv.conf
echo "nameserver 1.1.1.1" >> /etc/resolv.conf
java $JAVA_TOOL_OPTIONS -Xshare:on -XX:SharedArchiveFile=/workdir/app-cds.jsa  -jar /workdir/quarkus-run.jar
