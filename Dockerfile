FROM docker.io/alpine:3.19.1 as builder
RUN apk add --no-cache openjdk21

WORKDIR /workdir
COPY target/quarkus-app /workdir
RUN java -Xshare:dump -XX:DumpLoadedClassList=/workdir/classes.lst -cp /workdir/quarkus-run.jar
RUN java -Xshare:dump -XX:SharedClassListFile=/workdir/classes.lst -XX:SharedArchiveFile=/workdir/app-cds.jsa -cp /workdir/quarkus-run.jar

FROM docker.io/alpine:3.19.1

WORKDIR /workdir
COPY run.sh /workdir

RUN apk add --no-cache openjdk21 git ffmpeg curl python3 bind-tools net-tools
RUN curl -L https://github.com/yt-dlp/yt-dlp/releases/latest/download/yt-dlp -o /usr/local/bin/yt-dlp
RUN chmod a+rx /usr/local/bin/yt-dlp

COPY --from=builder /workdir /workdir

ENTRYPOINT ["/workdir/run.sh"]
