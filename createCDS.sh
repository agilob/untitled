java -Xshare:dump -XX:DumpLoadedClassList=classes.lst -cp target/quarkus-app/quarkus-run.jar
java -Xshare:dump -XX:SharedClassListFile=classes.lst -XX:SharedArchiveFile=app-cds.jsa -cp target/quarkus-app/quarkus-run.jar
java -Xshare:on -XX:SharedArchiveFile=app-cds.jsa -Xlog:class+load -jar target/quarkus-app/quarkus-run.jar
