CREATE EXTENSION IF NOT EXISTS "uuid-ossp";

CREATE CAST (varchar AS uuid)
  WITH INOUT
  AS IMPLICIT;

CREATE TABLE repository (
  repository_id        UUID PRIMARY KEY DEFAULT uuid_generate_v4(),
  name                 VARCHAR(255) UNIQUE,
  link                 VARCHAR(27500),
  save_path            VARCHAR(10000) NOT NULL,
  repository_type      VARCHAR(25) NOT NULL,
  schedule_frequency   VARCHAR(30) NOT NULL,
  version              INT NOT NULL,
  created_on           TIMESTAMP,
  updated              TIMESTAMP
);

CREATE INDEX repository_id ON repository(repository_id);

CREATE TABLE yt_dlp_settings (
  settings_id                    INT PRIMARY KEY DEFAULT 1,
  yt_dlp_config_file             VARCHAR(1000) NOT NULL,
  version                        INT NOT NULL
);
ALTER TABLE yt_dlp_settings ADD CONSTRAINT unique_yt_dlp_config UNIQUE (settings_id);
