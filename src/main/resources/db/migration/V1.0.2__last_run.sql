ALTER TABLE repository ADD COLUMN
  last_run TIMESTAMP;

ALTER TABLE repository ADD COLUMN
  last_successful_run TIMESTAMP;