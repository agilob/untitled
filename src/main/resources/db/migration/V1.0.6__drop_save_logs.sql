ALTER TABLE repository
    DROP COLUMN save_error_logs;

ALTER TABLE repository
     DROP COLUMN save_logs;