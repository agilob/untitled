ALTER TABLE repository ADD COLUMN
    save_logs boolean;

ALTER TABLE repository ADD COLUMN
    save_error_logs boolean;
