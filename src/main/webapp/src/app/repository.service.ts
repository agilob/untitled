import { isDevMode } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map, Observable } from 'rxjs';
import { Repository } from './repository/repository';
import * as moment from 'moment';

@Injectable({
  providedIn: 'root'
})
export class RepositoryService {

  headers = { 'content-type': 'application/json'}

  constructor(private http: HttpClient) { }

  public static getRepositoryUrl(): string {
    var hostAndPort: string;
    if(isDevMode()) {
      hostAndPort = window.location.hostname + ":8080";
    } else {
      hostAndPort = window.location.host;
    }
    return window.location.protocol + "//" + hostAndPort + "/repository"
  }

  getAll(): Observable<Array<Repository>> {
    var link = `${RepositoryService.getRepositoryUrl()}/all`;
    return this.http.get<Array<Repository>>(link)
      .pipe(map((res: Array<Repository>) => this.convertDateArrayFromServer(res)));
  }

  getById(id: string): Observable<Repository> {
    return this.http.get<Repository>(`${RepositoryService.getRepositoryUrl()}/${id}`);
  }

  startNow(id: string): Observable<boolean> {
    return this.http.get<boolean>(`${RepositoryService.getRepositoryUrl()}/${id}/start`);
  }

  post(repository: Repository): Observable<Repository>{
    const body = JSON.stringify(repository);
    return this.http.post<Repository>(`${RepositoryService.getRepositoryUrl()}`, body, {'headers': this.headers});
  }

  put(repository: Repository): Observable<Repository>{
    const body = JSON.stringify(repository);
    return this.http.put<Repository>(`${RepositoryService.getRepositoryUrl()}`, body, {'headers': this.headers});
  }

  deleteById(id: string): Observable<boolean> {
    return this.http.delete<boolean>(`${RepositoryService.getRepositoryUrl()}/${id}`);
  }

  private convertDateArrayFromServer(res: Array<Repository>): Array<Repository> {
    if (res) {
      res.forEach((topic: Repository) => {
        topic.lastRun = topic.lastRun ? moment(topic.lastRun) : undefined;
        topic.nextFireTime = topic.nextFireTime ? moment(topic.nextFireTime) : undefined;
        topic.lastSuccessfulRun = topic.lastSuccessfulRun ? moment(topic.lastSuccessfulRun) : undefined;
        topic.createdOn = topic.createdOn ? moment(topic.createdOn) : undefined;
        topic.updated = topic.updated ? moment(topic.updated) : undefined;
      });
    }
    return this.sortRepositories(res);
  }

  public sortRepositories(repos: Array<Repository>) {
    repos.sort(
      (repo1, repo2) => this.getTime(repo2.updated) - this.getTime(repo1.updated)
    );
    return repos;
  }

  private getTime(date?: moment.Moment): number {
    return date != null ? date.valueOf() : 0;
  }

}
