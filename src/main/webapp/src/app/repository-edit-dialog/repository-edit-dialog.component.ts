import { Component, Inject, ViewChild } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { RepositoryService } from '../repository.service';
import { Repository, RepositoryType, ScheduleFrequency } from '../repository/repository';
import * as moment from 'moment';

@Component({
  selector: 'app-repository-edit-dialog',
  templateUrl: './repository-edit-dialog.component.html',
  styleUrls: ['./repository-edit-dialog.component.scss']
})
export class RepositoryEditDialogComponent {

  public minDate: moment.Moment = moment();

  RepositoryType = RepositoryType;

  repoTypes = [
    RepositoryType.YT_DL,
    RepositoryType.GIT,
  ]

  ScheduleFrequency = ScheduleFrequency;

  schedules = [
    ScheduleFrequency.DAILY,
    ScheduleFrequency.WEEKLY,
    ScheduleFrequency.MONTHLY,
    ScheduleFrequency.ONCE,
  ]

  errorMessage: string | null = null;

  constructor(
    public dialogRef: MatDialogRef<RepositoryEditDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public repository: Repository,
    public repositoryService: RepositoryService
  ) {
    console.log(this.repository);
  }

  saveNewRepo(): void {

    this.errorMessage = null;
    if (this.repository.id) {
      this.repositoryService.put(this.repository).subscribe(
        (savedRepo: Repository) => {
          this.dialogRef.close(savedRepo);
        },
        (error) => {
          this.errorMessage = error.error;
        });
    } else {
      this.repositoryService.post(this.repository).subscribe(
        (savedRepo: Repository) => {
          this.dialogRef.close(savedRepo);
        },
        (error) => {
          this.errorMessage = error.error;
        });
    }
  }

  getSchedule(val: any): string {
    console.log(val);
    return `val`;
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

}
