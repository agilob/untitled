import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RepositoryEditDialogComponent } from './repository-edit-dialog.component';

describe('RepositoryEditDialogComponent', () => {
  let component: RepositoryEditDialogComponent;
  let fixture: ComponentFixture<RepositoryEditDialogComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RepositoryEditDialogComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RepositoryEditDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
