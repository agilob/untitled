import { Moment } from "moment";

export interface Repository {
  id: string;
  name: string;
  link: string;
  savePath: string;
  repositoryType: RepositoryType;
  scheduleFrequency: ScheduleFrequency;
  startDateTime?: Moment;
  isRunning: boolean;
  lastRun?: Moment;
  nextFireTime?: Moment;
  lastSuccessfulRun?: Moment;
  createdOn?: Moment;
  updated?: Moment;
}

export enum RepositoryType {
  YT_DL = 'YT_DL',
  GIT = 'GIT',
  // SVN = 'SVN',
}

export enum ScheduleFrequency {
  ONCE = 'ONCE',
  DAILY = 'DAILY',
  // BIDAILY = 'BIDAILY',
  WEEKLY = 'WEEKLY',
  // BIWEEKLY = 'BIWEEKLY',
  MONTHLY = 'MONTHLY'
}
