import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { RepositoryService } from '../repository.service';
import { Repository, RepositoryType, ScheduleFrequency } from '../repository/repository';
import { animate, state, style, transition, trigger } from '@angular/animations';
import { RepositoryEditDialogComponent } from '../repository-edit-dialog/repository-edit-dialog.component';
import { MatTableDataSource } from '@angular/material/table';
import { faPen, faTrash, faPlay } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
  animations: [
    trigger('detailExpand', [
      state('collapsed', style({height: '0px', minHeight: '0'})),
      state('expanded', style({height: '*'})),
      transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
    ]),
  ],
})
export class HomeComponent implements OnInit {

  faCoffee = faTrash;
  faEdit = faPen;
  faPlay = faPlay;

  columnsToDisplay: string[] = ['name', 'link', 'savePath', 'repositoryType', 'customColumn1'];

  repoColumnsToDisplay: string[] = ['name', 'link', 'savePath', 'repositoryType'];

  repositories = new MatTableDataSource<Repository>([])

  newRepo = {} as Repository;

  expandedElement: Repository | null;

  constructor(
    public repositoryService: RepositoryService,
    public dialog: MatDialog) { }

  ngOnInit(): void {
    this.refreshAll();
  }

  openDialog(): void {
    const dialogRef = this.dialog.open(RepositoryEditDialogComponent, {
      width: '600px',
      data: this.copyOfRepo({
        repositoryType: RepositoryType.YT_DL,
        scheduleFrequency: ScheduleFrequency.WEEKLY
      } as Repository)
    });

    dialogRef.afterClosed().subscribe((result: Repository) => {
      if (result != null && result.id != null) {
        this.repositories.data.push(result);
        this.repositories.data = this.repositoryService.sortRepositories(this.repositories.data);
      }
    });
  }

  copyOfRepo(element: Repository): Repository {
    return {...element } as Repository;
  }

  edit(repository: Repository): void {
    const dialogRef = this.dialog.open(RepositoryEditDialogComponent, {
      width: '600px',
      data: repository,
    });

    dialogRef.afterClosed().subscribe((result: Repository) => {
      console.debug(JSON.stringify(result));
      if (result != null && result.id != null) {
        this.refreshAll();
      }
    });
  }

  delete(repository: Repository): void {
    this.repositoryService.deleteById(repository.id).subscribe((x: boolean) => {
      if (x) {
        this.refreshAll();
      } else {
        console.log(`Delete by id ${repository.id} failed, check server logs`);

      }
    })
  }

  startNow(repository: Repository): void {
    this.repositoryService.startNow(repository.id).subscribe((x: boolean) => {
        if (x) {
          console.log(`Schedule repo job for now successfully`);
        } else {
          console.log(`Delete by id ${repository.id} failed, check server logs`);
        }
      })
  }

  canScheduleRunNow(repository: Repository): boolean {
    return repository.scheduleFrequency === ScheduleFrequency.ONCE || repository.isRunning === true;
  }

  private refreshAll(): void {
    this.repositories.data = [];
    this.repositoryService.getAll().subscribe((x: Array<Repository>) => {
      x.forEach(element => {
        this.repositories.data.push(element);
      });
    })
  }

}
