package net.agilob.untitled.config;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Executors;

import org.eclipse.microprofile.config.inject.ConfigProperty;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import io.quarkus.runtime.StartupEvent;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.enterprise.event.Observes;
import net.agilob.untitled.quartz.LogsReader;

@ApplicationScoped
public class YtDlpUpdater {

    private static final Logger LOGGER = LoggerFactory.getLogger(YtDlpUpdater.class);

    private static final String UPGRADE_NAME = YtDlpUpdater.class.getSimpleName();

    @ConfigProperty(name = "untitled.ytdlp.selfupdate")
    public boolean updateYtDlp;

    public void onStart(@Observes StartupEvent ev) {
        if (updateYtDlp) {
            LOGGER.error("Updating yt-dlp");

            final List<String> list = new ArrayList<>(2);
            list.add("yt-dlp");
            list.add("--update");

            final ProcessBuilder builder = new ProcessBuilder(list);
            builder.redirectOutput(ProcessBuilder.Redirect.PIPE);
            builder.redirectError(ProcessBuilder.Redirect.PIPE);

            Process process;
            try {
                process = builder.start();

                final LogsReader stdReader = new LogsReader(UPGRADE_NAME, process.getInputStream(), true);
                Executors.newSingleThreadExecutor().submit(stdReader);

                final LogsReader errReader = new LogsReader(UPGRADE_NAME, process.getErrorStream(), true);
                Executors.newSingleThreadExecutor().submit(errReader);

                process.waitFor();
            } catch (IOException | InterruptedException e) {
                LOGGER.error("Failed upgrading yt-dlp {}", e.getMessage());
            }
            LOGGER.error("Update of yt-dlp completed");
        }
    }
}
