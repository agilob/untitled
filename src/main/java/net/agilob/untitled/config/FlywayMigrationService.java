package net.agilob.untitled.config;

import org.flywaydb.core.Flyway;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import jakarta.enterprise.context.ApplicationScoped;
import jakarta.inject.Inject;

@ApplicationScoped
public class FlywayMigrationService {

    private static final Logger LOGGER = LoggerFactory.getLogger(FlywayMigrationService.class);

    @Inject
    Flyway flyway;

    public void checkMigration() {
        flyway.clean();
        flyway.migrate();
        LOGGER.debug("Flyway migrated {}", flyway.info().current().getVersion().toString());
    }
}
