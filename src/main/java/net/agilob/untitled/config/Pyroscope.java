package net.agilob.untitled.config;

import java.util.Optional;

import org.eclipse.microprofile.config.inject.ConfigProperty;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import io.pyroscope.http.Format;
import io.pyroscope.javaagent.EventType;
import io.pyroscope.javaagent.PyroscopeAgent;
import io.pyroscope.javaagent.api.Logger.Level;
import io.pyroscope.javaagent.config.Config;
import io.quarkus.runtime.StartupEvent;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.enterprise.event.Observes;

@ApplicationScoped
public class Pyroscope {

    private static final Logger LOGGER = LoggerFactory.getLogger(Pyroscope.class);

    @ConfigProperty(name = "quarkus.application.name")
    public String appName;

    @ConfigProperty(name = "untitled.pyroscope.enabled")
    public boolean enabled;

    @ConfigProperty(name = "untitled.pyroscope.server")
    public Optional<String> pyroscopeServer;

    @ConfigProperty(name = "untitled.pyroscope.auth_token")
    public Optional<String> pyroscopeAuthToken;

    public void onStart(@Observes StartupEvent ev) {
        if (enabled) {
            if (pyroscopeServer.isEmpty() || pyroscopeServer.get() == null || pyroscopeServer.get().trim().isEmpty()) {
                LOGGER.error("Pyroscope server config var is empty. Skipping Pyroscope configuration.");
                return;
            }
            LOGGER.debug("Starting pyroscope");
            PyroscopeAgent.start(
                    new Config.Builder()
                            .setApplicationName(appName)
                            .setProfilingEvent(EventType.ITIMER)
                            .setFormat(Format.JFR)
                            .setLogLevel(Level.ERROR)
                            .setServerAddress(pyroscopeServer.get())
                            .setAuthToken(pyroscopeAuthToken.orElse(""))
                            .build());
            LOGGER.debug("Pyroscope started");
        }
    }
}
