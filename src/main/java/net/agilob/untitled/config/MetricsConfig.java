package net.agilob.untitled.config;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.List;

import io.micrometer.core.instrument.Tag;
import io.micrometer.core.instrument.config.MeterFilter;
import jakarta.enterprise.inject.Produces;
import jakarta.inject.Singleton;

@Singleton

public class MetricsConfig {

    /** Define common tags that apply globally */
    @Produces
    @Singleton
    public MeterFilter configureAllRegistries() {
        String podName = "unknown";
        try {
            podName = InetAddress.getLocalHost().getHostName();
        } catch (UnknownHostException uhe) {

        }
        return MeterFilter.commonTags(
            List.of(
                Tag.of("application", "untitled"),
                Tag.of("pod", podName)
                )
            );
    }

}
