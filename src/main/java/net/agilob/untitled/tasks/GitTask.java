package net.agilob.untitled.tasks;

import io.quarkus.arc.Arc;
import io.quarkus.arc.ManagedContext;
import net.agilob.untitled.quartz.JobManager;
import net.agilob.untitled.repository.Repository;
import net.agilob.untitled.repository.RepositoryRepo;
import net.agilob.untitled.util.DirectoryUtil;
import org.eclipse.jgit.api.Git;
import org.eclipse.jgit.api.errors.GitAPIException;
import org.eclipse.jgit.lib.TextProgressMonitor;
import org.eclipse.jgit.storage.file.FileRepositoryBuilder;
import org.quartz.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;

@DisallowConcurrentExecution
@PersistJobDataAfterExecution
public class GitTask implements Job {

    private static final Logger LOGGER = LoggerFactory.getLogger(GitTask.class);

    @Override
    public void execute(JobExecutionContext context) throws JobExecutionException {
        ManagedContext requestContext = Arc.container().requestContext();
        if (!requestContext.isActive()) {
            requestContext.activate();
        }

        final var repositoryRepo = Arc.container().instance(RepositoryRepo.class).get();

        final String repositoryId = (String) context.getJobDetail().getJobDataMap().get(JobManager.DATA_KEY);
        final Repository repo = repositoryRepo.findById(repositoryId);

        LOGGER.debug("Attempting to scan git repo {} / {} for new commits",
                repo.name, repo.link);

        final File savePath = DirectoryUtil.createDirectory(repo.savePath);

        try {

            FileRepositoryBuilder repositoryBuilder = new FileRepositoryBuilder();
            repositoryBuilder.findGitDir(savePath);

            if (repositoryBuilder.getGitDir() != null) {
                LOGGER.info("Pulling new commits from repo {}", repo.link);

                try(var gitCommand = Git.open(savePath)) {
                    gitCommand.pull()
                            .setProgressMonitor(new TextProgressMonitor())
                            .call();
                }
            } else {
                LOGGER.info("Cloning repo {}", repo.link);

                try(var gitResult = Git.cloneRepository()
                        .setURI(repo.link)
                        .setDirectory(savePath)
                        .setCloneAllBranches(true)
                        .setCloneSubmodules(true)
                        .setProgressMonitor(new TextProgressMonitor())
                        .call()) {
                    LOGGER.info("Finished cloning repo {}:{}",
                            gitResult.getRepository().getIdentifier(),
                            gitResult.getRepository().getFullBranch());
                }
            }

        } catch (GitAPIException | IOException ioe) {
            LOGGER.error("Failed process repo {}: {}", repo.name, ioe.getMessage());
            throw new JobExecutionException(ioe);
        } finally {
            LOGGER.info("Finished processing GitTask for repo {}", repo.name);
        }
    }
}
