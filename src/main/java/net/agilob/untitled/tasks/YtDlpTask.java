package net.agilob.untitled.tasks;

import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.Executors;

import org.eclipse.microprofile.config.inject.ConfigProperty;
import org.quartz.DisallowConcurrentExecution;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.PersistJobDataAfterExecution;
import org.quartz.utils.FindbugsSuppressWarnings;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import io.quarkus.arc.Arc;
import io.quarkus.arc.ManagedContext;
import net.agilob.untitled.quartz.JobManager;
import net.agilob.untitled.quartz.LogsReader;
import net.agilob.untitled.repository.Repository;
import net.agilob.untitled.repository.RepositoryRepo;
import net.agilob.untitled.repository.RepositoryResource;
import net.agilob.untitled.repository.ScheduleFrequency;
import net.agilob.untitled.util.DirectoryUtil;
import net.agilob.untitled.ytdlp.YtDlpProcesses;

@DisallowConcurrentExecution
@PersistJobDataAfterExecution
public class YtDlpTask implements Job {

    private static final Logger LOGGER = LoggerFactory.getLogger(YtDlpTask.class);

    @ConfigProperty(name = "ytdlp.config")
    Optional<String> configFile;

    @Override
    @FindbugsSuppressWarnings("COMMAND_INJECTION")
    public void execute(JobExecutionContext jobExecutionContext) throws JobExecutionException {
        ManagedContext requestContext = Arc.container().requestContext();
        if (!requestContext.isActive()) {
            requestContext.activate();
        }

        final var repositoryRepo = Arc.container().instance(RepositoryRepo.class).get();
        final var ytdlpProcesses = Arc.container().instance(YtDlpProcesses.class).get();

        final String repositoryId = (String) jobExecutionContext.getJobDetail().getJobDataMap()
                .get(JobManager.DATA_KEY);
        final Repository repo = repositoryRepo.findById(repositoryId);

        LOGGER.debug("Attempting to scan yt-dlp repo {} / {} for new media",
                repo.name, repo.link);

        try {
            final List<String> processParams = new ArrayList<>();
            processParams.add("yt-dlp");
            processParams.add(repo.link);

            if (LOGGER.isDebugEnabled()) {
                LOGGER.debug("Will execute command: `{}` in directory {}",
                        String.join(" ", processParams), repo.savePath);
            }

            final File savePath = DirectoryUtil.createDirectory(repo.savePath);

            appendYtDlpConfigLocation(savePath, processParams);

            LOGGER.debug("Directory {} exists: {} and isDirectory: {}",
                    savePath.getAbsolutePath(), savePath.exists(), savePath.isDirectory());

            final ProcessBuilder builder = new ProcessBuilder(processParams);
            builder.directory(savePath);

            builder.redirectOutput(ProcessBuilder.Redirect.PIPE);
            builder.redirectError(ProcessBuilder.Redirect.PIPE);

            final Process process = builder.start();

            final LogsReader stdReader = new LogsReader(repo.name, process.getInputStream(), false);
            Executors.newSingleThreadExecutor().submit(stdReader);

            final LogsReader errReader = new LogsReader(repo.name, process.getErrorStream(), true);
            Executors.newSingleThreadExecutor().submit(errReader);

            ytdlpProcesses.addProcess(process);

            final int exitCode = process.waitFor();

            if (exitCode == 0) {
                repositoryRepo.updateLastSuccessfulRun(repositoryId);
                if (repo.scheduleFrequency == ScheduleFrequency.ONCE) {
                    Arc.container().instance(RepositoryResource.class).get().deleteById(repositoryId);
                }
            } else {
                throw new JobExecutionException("Execution failed with exit code " + exitCode);
            }

        } catch (RuntimeException | IOException | InterruptedException e) {
            LOGGER.error("Failed process repo {}: {}", repo.name, e.getMessage());
            throw new JobExecutionException(e);
        } finally {
            LOGGER.info("Finished processing task for channel {}", repo.name);
        }
    }

    void appendYtDlpConfigLocation(final File savePath, final List<String> processParams) {

        boolean containsLocalConfig = false;

        final var files = savePath.listFiles();
        if(files != null) {
            for (final File f : files) {
                if (f.getName().equals("yt-dlp.conf") && f.isFile()) {
                    LOGGER.debug("Found directory specific yt-dlp config in {}", savePath.getAbsolutePath());
                    containsLocalConfig = true;
                    break;
                }
            }
        }

        if (containsLocalConfig) {
            processParams.add("--config-location");
            processParams.add(savePath + "/yt-dlp.conf");
        } else {
            configFile.ifPresent(config -> {
                final var file = Path.of(config).toFile();
                if (file.exists() && file.isFile()) {
                    processParams.add("--config-location");
                    processParams.add(config);
                } else {
                    LOGGER.error("File with yt-dlp config path {} no longer exists or is not a file",
                            configFile);
                }
            });
        }
    }
}
