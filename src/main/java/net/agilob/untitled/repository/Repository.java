package net.agilob.untitled.repository;

import java.time.LocalDateTime;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import io.quarkus.hibernate.orm.panache.PanacheEntityBase;
import jakarta.persistence.Cacheable;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import jakarta.persistence.Version;
import jakarta.validation.constraints.NotEmpty;

@Entity
@Table(name = "repository")
@Cacheable
public class Repository extends PanacheEntityBase {

    @Id
    @Column(name = "repository_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public String id;

    @Column(name = "name", length = 255, nullable = false)
    public String name;

    @NotEmpty
    @Column(name = "link", length = 27_500, nullable = false)
    public String link;

    @NotEmpty
    @Column(name = "save_path", length = 10_000, nullable = false)
    public String savePath;

    @Enumerated(EnumType.STRING)
    @Column(name = "repository_type", length = 25, nullable = false)
    public RepositoryType repositoryType;

    @Enumerated(EnumType.STRING)
    @Column(name = "schedule_frequency", length = 30)
    public ScheduleFrequency scheduleFrequency;

    @Column(name = "last_run")
    public LocalDateTime lastRun;

    @Column(name = "last_successful_run")
    public LocalDateTime lastSuccessfulRun;

    @Version
    public int version;

    @CreationTimestamp
    @Column(name = "created_on")
    public LocalDateTime createdOn;

    @UpdateTimestamp
    @Column(name = "updated")
    public LocalDateTime updated;

    // TODO: equals, hashCOde and toString
}
