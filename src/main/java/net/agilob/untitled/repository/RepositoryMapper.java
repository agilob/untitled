package net.agilob.untitled.repository;

import net.agilob.untitled.EntityMapper;
import org.mapstruct.Mapper;

@Mapper(componentModel = "jakarta")
public interface RepositoryMapper extends EntityMapper<Repository, RepositoryDTO> {

}

