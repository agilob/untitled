package net.agilob.untitled.repository;

public enum ScheduleFrequency {
    ONCE,
    DAILY,
    BIDAILY,
    WEEKLY,
    BIWEEKLY,
    MONTHLY
}
