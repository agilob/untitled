package net.agilob.untitled.repository;

import java.time.LocalDateTime;

import jakarta.enterprise.context.ApplicationScoped;
import jakarta.transaction.Transactional;

import io.quarkus.hibernate.orm.panache.PanacheRepositoryBase;

@ApplicationScoped
public class RepositoryRepo implements PanacheRepositoryBase<Repository, String> {

    @Transactional
    public void save(Repository repository) {
        persist(repository);
    }

    @Transactional
    public boolean deleteByID(final String id) {
        return deleteById(id);
    }

    @Transactional
    public void updateLastRun(final String id) {
        update("UPDATE FROM Repository SET lastRun = ?1 WHERE id = ?2", LocalDateTime.now(), id);
    }

    @Transactional
    public void updateLastSuccessfulRun(final String id) {
        update("UPDATE FROM Repository SET lastSuccessfulRun = ?1 WHERE id = ?2", LocalDateTime.now(), id);
    }
}
