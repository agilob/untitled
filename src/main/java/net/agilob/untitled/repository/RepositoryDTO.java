package net.agilob.untitled.repository;

import java.time.LocalDateTime;
import java.util.Objects;

import jakarta.validation.constraints.Future;
import jakarta.validation.constraints.NotNull;
import org.quartz.utils.FindbugsSuppressWarnings;

@FindbugsSuppressWarnings("URF_UNREAD_PUBLIC_OR_PROTECTED_FIELD")
public class RepositoryDTO {

    public String id;

    @NotNull(message = "name is required")
    public String name;

    @NotNull(message = "link is required")
    public String link;

    @NotNull(message = "savePath is required")
    public String savePath;

    @NotNull(message = "repositoryType is required")
    public RepositoryType repositoryType;

    @NotNull(message = "scheduleFrequency is required")
    public ScheduleFrequency scheduleFrequency;

    @Future(message = "startDateTime must be in the future")
    public LocalDateTime startDateTime;

    public boolean isRunning;

    public LocalDateTime nextFireTime;

    public LocalDateTime lastRun;

    public LocalDateTime lastSuccessfulRun;

    public LocalDateTime createdOn;

    public LocalDateTime updated;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        RepositoryDTO that = (RepositoryDTO) o;
        return Objects.equals(id, that.id) && Objects.equals(name, that.name) && Objects.equals(link, that.link) && Objects.equals(savePath, that.savePath) && repositoryType == that.repositoryType && scheduleFrequency == that.scheduleFrequency && Objects.equals(startDateTime, that.startDateTime) && Objects.equals(nextFireTime, that.nextFireTime) && Objects.equals(lastRun, that.lastRun) && Objects.equals(lastSuccessfulRun, that.lastSuccessfulRun) && Objects.equals(createdOn, that.createdOn) && Objects.equals(updated, that.updated);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, link, savePath, repositoryType, scheduleFrequency, startDateTime, nextFireTime, lastRun, lastSuccessfulRun, createdOn, updated);
    }

    @Override
    public String toString() {
        return "RepositoryDTO{" +
                "id='" + id + '\'' +
                ", name='" + name + '\'' +
                ", link='" + link + '\'' +
                ", savePath='" + savePath + '\'' +
                ", repositoryType=" + repositoryType +
                ", scheduleFrequency=" + scheduleFrequency +
                ", startDateTime=" + startDateTime +
                ", nextFireTime=" + nextFireTime +
                ", lastRun=" + lastRun +
                ", lastSuccessfulRun=" + lastSuccessfulRun +
                ", createdOn=" + createdOn +
                ", updated=" + updated +
                '}';
    }
}
