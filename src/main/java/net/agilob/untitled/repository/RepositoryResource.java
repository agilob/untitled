package net.agilob.untitled.repository;

import net.agilob.untitled.quartz.JobManager;
import net.agilob.untitled.util.DirectoryUtil;
import org.quartz.SchedulerException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import jakarta.inject.Inject;
import jakarta.transaction.Transactional;
import jakarta.validation.Valid;
import jakarta.ws.rs.*;
import jakarta.ws.rs.core.MediaType;
import java.io.File;
import java.util.Collections;
import java.util.List;

@Path("/repository")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class RepositoryResource {

    private static final Logger LOGGER = LoggerFactory.getLogger(RepositoryResource.class);

    @Inject
    RepositoryRepo repositoryRepo;

    @Inject
    RepositoryMapper repositoryMapper;

    @Inject
    JobManager jobManager;

    @GET
    @Path("/all")
    @Transactional
    public List<RepositoryDTO> getAllChannels() {

        List<RepositoryDTO> repositoryDTOS = repositoryMapper.toDto(repositoryRepo.listAll());
        jobManager.retrieveNextFireTimeForJobs(repositoryDTOS);
        jobManager.checkJobIsRunning(repositoryDTOS);
        return repositoryDTOS;
    }

    @GET
    @Path("/{repositoryId}")
    public RepositoryDTO getById(@PathParam("repositoryId") final String repoId) {
        LOGGER.debug("Getting by id {}", repoId);
        try {
            final var repo = repositoryRepo.findById(repoId);
            if(repo == null) {
                throw new NotFoundException("Repository with id " + repoId + " not found");
            }
            RepositoryDTO repositoryDTO = repositoryMapper.toDto(repo);
            jobManager.retrieveNextFireTimeForJobs(Collections.singleton(repositoryDTO));
            return repositoryDTO;
        } catch (Exception e) {
            LOGGER.error("Failed to find repository by ID {}", repoId, e);
            throw new NotFoundException("Repository with id " + repoId + " not found");
        }
    }

    @POST
    @Path("/")
    public RepositoryDTO createRepository(@Valid final RepositoryDTO repository) {
        LOGGER.debug("Creating {}", repository);
        DirectoryUtil.createDirectory(repository.savePath);

        Repository repo = repositoryMapper.toEntity(repository);

        repositoryRepo.save(repo);
        try {
            jobManager.createJob(repo.id, repository);
        } catch (SchedulerException e) {
            LOGGER.error("{}", e.getMessage());
            return null;
        }

        RepositoryDTO repositoryDTO = repositoryMapper.toDto(repo);
        jobManager.retrieveNextFireTimeForJobs(Collections.singleton(repositoryDTO));
        return repositoryDTO;
    }

    @PUT
    @Path("/")
    @Transactional
    public RepositoryDTO updateRepository(@Valid final RepositoryDTO repoDTO) {
        LOGGER.info("Updating {}", repoDTO);
        DirectoryUtil.createDirectory(repoDTO.savePath);

        jobManager.deleteJob(repoDTO);

        Repository repo = repositoryRepo.findById(repoDTO.id);
        try {
            jobManager.createJob(repoDTO.id, repoDTO);

            repo.name = repoDTO.name;
            repo.link = repoDTO.link;
            repo.repositoryType = repoDTO.repositoryType;
            repo.savePath = repoDTO.savePath;
            repo.scheduleFrequency = repoDTO.scheduleFrequency;
            repositoryRepo.save(repo);

            LOGGER.debug("Repo updated {}", repo);
        } catch (SchedulerException e) {
            LOGGER.error("{}", e.getMessage());
            return null;
        }

        RepositoryDTO repositoryDTO = repositoryMapper.toDto(repo);
        jobManager.retrieveNextFireTimeForJobs(Collections.singleton(repositoryDTO));
        return repositoryDTO;
    }

    @DELETE
    @Path("/{repositoryId}")
    public boolean deleteById(@PathParam("repositoryId") final String repoId) {
        LOGGER.debug("Deleting by id {}", repoId);
        try {
            final var repo = repositoryRepo.findById(repoId);
            LOGGER.debug("Deleting repo {}", repo);
            jobManager.deleteJob(repositoryMapper.toDto(repo));
            return repositoryRepo.deleteByID(repoId);
        } catch (Exception e) {
            LOGGER.error("Failed to delete repository by ID {}", repoId);
            return false;
        }
    }

    @GET
    @Path("/{repositoryId}/start")
    public boolean startNow(@PathParam("repositoryId") final String repoId) {
        LOGGER.info("Triggering job by repo id {}", repoId);
        final var repo = repositoryRepo.findById(repoId);
        if(repo == null) {
            return false;
        }
        if(repo.scheduleFrequency == ScheduleFrequency.ONCE) {
            return false;
        }

        final var repoDto = repositoryMapper.toDto(repo);
        return jobManager.startJobNow(repoDto);
    }


}
