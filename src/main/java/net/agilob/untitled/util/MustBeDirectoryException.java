package net.agilob.untitled.util;

public class MustBeDirectoryException extends RuntimeException {
    public MustBeDirectoryException(String filePath) {
        super("Save path " + filePath + " must be a writable directory");
    }
}
