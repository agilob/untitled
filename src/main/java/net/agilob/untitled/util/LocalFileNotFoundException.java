package net.agilob.untitled.util;

public class LocalFileNotFoundException extends RuntimeException {
    public LocalFileNotFoundException(String errorMessage) {
        super(errorMessage);
    }
}
