package net.agilob.untitled.util;

import org.quartz.utils.FindbugsSuppressWarnings;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.nio.file.Path;
import java.sql.Savepoint;

public final class DirectoryUtil {

    private static final Logger LOGGER = LoggerFactory.getLogger(DirectoryUtil.class);

    @FindbugsSuppressWarnings(value="PATH_TRAVERSAL_IN")
    public static File createDirectory(final String savePathString) {
        if(savePathString.length() == 0) {
            throw new MustBeDirectoryException("Must be an absolute directory path");
        }
        if(savePathString.startsWith(File.separator) == false) {
            throw new MustBeDirectoryException("Must be an absolute directory path");
        }
        final File savePath = Path.of(savePathString).normalize().toFile();

        if(savePath.isAbsolute() == false) {
            throw new MustBeDirectoryException("Must be an absolute directory path");
        }
        if(savePath.exists() == false) {
            LOGGER.warn("Directory {} doesn't exist, creating it", savePath.getAbsolutePath());
            final boolean created = savePath.mkdirs();
            if (created == false) {
                final String message = "Failed to create " + savePath.getAbsolutePath() + " directory!";
                LOGGER.error(message);
                LOGGER.error("Check if path exists or untitled has permissions to create the missing directory");
                throw new MustBeDirectoryException(savePath.getAbsolutePath());
            }
        } else if(savePath.isFile()) {
            throw new MustBeDirectoryException(savePath.getAbsolutePath());
        }
        return savePath;
    }
}
