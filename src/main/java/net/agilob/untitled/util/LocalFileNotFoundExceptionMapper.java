package net.agilob.untitled.util;

import jakarta.ws.rs.core.Response;
import jakarta.ws.rs.ext.ExceptionMapper;
import jakarta.ws.rs.ext.Provider;

@Provider
public class LocalFileNotFoundExceptionMapper implements ExceptionMapper<LocalFileNotFoundException> {
    @Override
    public Response toResponse(LocalFileNotFoundException exception) {
        return Response.status(400).entity(exception.getMessage()).build();
    }
}
