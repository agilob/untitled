package net.agilob.untitled.ytdlp;

import jakarta.enterprise.context.ApplicationScoped;
import org.quartz.utils.FindbugsSuppressWarnings;

import java.util.ArrayList;
import java.util.List;

@ApplicationScoped
public class YtDlpProcesses {

    private final List<Process> processes;

    public YtDlpProcesses() {
        this.processes = new ArrayList<>(0);
    }

    public synchronized void addProcess(final Process process) {
        this.processes.add(process);
    }

    @FindbugsSuppressWarnings("EI_EXPOSE_REP")
    public synchronized List<Process> getProcesses() {
        return this.processes;
    }

}
