package net.agilob.untitled.ytdlp;

import io.quarkus.runtime.ShutdownEvent;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.enterprise.event.Observes;
import jakarta.inject.Inject;
import net.agilob.untitled.tasks.YtDlpTask;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@ApplicationScoped
public class YtDlpExecutors {

    private static final Logger LOGGER = LoggerFactory.getLogger(YtDlpTask.class);

    @Inject
    public YtDlpProcesses ytDlpProcesses;

    public void onStop(@Observes ShutdownEvent ev) {
        LOGGER.warn("Detected application shutdown request, stopping all YtDlp processes");

        for (Process process : ytDlpProcesses.getProcesses()) {
            LOGGER.warn("Destroying process {}", process.info().command());
            process.destroy();
        }
    }

}
