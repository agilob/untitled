package net.agilob.untitled.quartz;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.function.BiConsumer;

public final class LogsReader implements Runnable {

    private static final Logger LOGGER = LoggerFactory.getLogger(LogsReader.class);

    private final InputStream inputStream;
    private final String channelName;
    private final BiConsumer<String, String> consumer;

    public LogsReader(final String channelName, final InputStream inputStream, final boolean isError) {
        this.channelName = channelName;

        if(isError) {
            this.consumer = this::consumeError;
        } else {
            this.consumer = this::consumeLog;
        }

        this.inputStream = inputStream;
    }

    @Override
    public void run() {
        new BufferedReader(new InputStreamReader(inputStream, StandardCharsets.UTF_8)).lines()
                .forEach(line -> this.consumer.accept(channelName, line));
    }

    private void consumeLog(final String channelId, final String log) {
        LOGGER.debug("{} : {}", channelId, log);
    }

    private void consumeError(final String channelId, final String log) {
        LOGGER.warn("{} : {}",  channelId, log);
    }
}
