package net.agilob.untitled.quartz;

import java.time.Duration;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.temporal.ChronoUnit;
import java.util.*;

import jakarta.enterprise.context.ApplicationScoped;
import jakarta.inject.Inject;
import net.agilob.untitled.repository.RepositoryType;
import net.agilob.untitled.tasks.GitTask;
import org.quartz.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import net.agilob.untitled.repository.RepositoryDTO;
import net.agilob.untitled.repository.ScheduleFrequency;
import net.agilob.untitled.ytdlp.YtTask;

@ApplicationScoped
public class JobManager {

    public static final String DATA_KEY = "repository_id";

    private static final Logger LOGGER = LoggerFactory.getLogger(JobManager.class);

    /**
     * I think these two states mean the job is running
     */
    private static final Set<Trigger.TriggerState> RUNNING_STATES = EnumSet.of(Trigger.TriggerState.PAUSED,
            Trigger.TriggerState.BLOCKED);

    @Inject
    Scheduler scheduler;

    public LocalDateTime createJob(final String id, final RepositoryDTO repositoryDTO) throws SchedulerException {
        Class<? extends Job> jobType;

        if (repositoryDTO.repositoryType == RepositoryType.YT_DL) {
            jobType = YtTask.class;
        } else if (repositoryDTO.repositoryType == RepositoryType.GIT) {
            jobType = GitTask.class;
        } else {
            throw new SchedulerException("RepositoryType has no valid value");
        }

        final JobDetail job = JobBuilder.newJob(jobType)
                .withIdentity(repositoryDTO.name, repositoryDTO.repositoryType.toString())
                .withDescription(repositoryDTO.link)
                .usingJobData(DATA_KEY, id)
                .requestRecovery()
                .build();

        final var trigger = createTrigger(repositoryDTO);

        scheduler.scheduleJob(job, trigger);

        return LocalDateTime.ofInstant(
                Instant.ofEpochMilli(trigger.getNextFireTime().getTime()),
                ZoneId.systemDefault());
    }

    public boolean deleteJob(final RepositoryDTO repositoryDTO) {
        final JobKey jk = new JobKey(repositoryDTO.name, repositoryDTO.repositoryType.toString());
        try {
            return scheduler.deleteJob(jk);
        } catch (SchedulerException e) {
            LOGGER.error("Failed to delete job by jobKey {}, because: {}", jk, e.getMessage(), e);
            return false;
        }
    }

    public void retrieveNextFireTimeForJobs(final Collection<RepositoryDTO> repositoriesDto) {
        for (final RepositoryDTO repoDto : repositoriesDto) {
            if (this.getJobKeyKey(repoDto).isPresent()) {
                final var trigger = this.getJobKeyKey(repoDto).get();
                if (repoDto.scheduleFrequency != ScheduleFrequency.ONCE) {
                    repoDto.nextFireTime = LocalDateTime.ofInstant(
                            Instant.ofEpochMilli(trigger.getNextFireTime().getTime()),
                            ZoneId.systemDefault());
                }
            }
        }
    }

    public void checkJobIsRunning(final Collection<RepositoryDTO> repositoriesDto) {
        for (final RepositoryDTO repoDto : repositoriesDto) {
            try {
                repoDto.isRunning = isJobPaused(repoDto);
            } catch (SchedulerException e) {
                LOGGER.error("Failed to get job isRunning status {}", e.getMessage(), e);
            }
        }
    }

    private boolean isJobPaused(final RepositoryDTO repositoryDTO) throws SchedulerException {

        final var jobKey = JobKey.jobKey(repositoryDTO.name, repositoryDTO.repositoryType.toString());
        final JobDetail jobDetail = scheduler.getJobDetail(jobKey);
        if (jobDetail == null) {
            LOGGER.error("Unexpectedly job detail for {}:{} is not found", repositoryDTO.id, repositoryDTO.name);
            return false;
        }
        final List<? extends Trigger> triggers = scheduler.getTriggersOfJob(jobDetail.getKey());
        for (final Trigger trigger : triggers) {
            Trigger.TriggerState triggerState = scheduler.getTriggerState(trigger.getKey());
            if (RUNNING_STATES.contains(triggerState)) {
                return true;
            }
        }
        return false;
    }

    private Trigger createTrigger(final RepositoryDTO repositoryDTO) {
        Date startDate;
        if (repositoryDTO.startDateTime == null) {
            startDate = Date.from(LocalDateTime.now().plus(
                    Duration.of(1, ChronoUnit.MINUTES))
                    .atZone(ZoneId.systemDefault()).toInstant());
        } else {
            startDate = Date.from(repositoryDTO.startDateTime.atZone(ZoneId.systemDefault()).toInstant());
        }

        if (repositoryDTO.scheduleFrequency == ScheduleFrequency.ONCE) {
            return TriggerBuilder.newTrigger()
                    .withIdentity(repositoryDTO.name, repositoryDTO.repositoryType.toString())
                    .startAt(startDate)
                    .withSchedule(SimpleScheduleBuilder.simpleSchedule()
                            .withMisfireHandlingInstructionFireNow())
                    .build();
        } else {
            return TriggerBuilder.newTrigger()
                    .withIdentity(repositoryDTO.name, repositoryDTO.repositoryType.toString())
                    .startAt(startDate)
                    .withSchedule(
                            createSchedule(repositoryDTO.scheduleFrequency)
                                    .withMisfireHandlingInstructionFireNow())
                    .build();
        }
    }

    private Optional<Trigger> getJobKeyKey(final RepositoryDTO repositoryDTO) {
        try {
            return Optional.ofNullable(scheduler
                    .getTrigger(TriggerKey.triggerKey(repositoryDTO.name, repositoryDTO.repositoryType.toString())));
        } catch (SchedulerException e) {
            return Optional.empty();
        }
    }

    public boolean startJobNow(final RepositoryDTO repositoryDTO) {
        final var jobKey = JobKey.jobKey(repositoryDTO.name, repositoryDTO.repositoryType.toString());

        try {
            scheduler.triggerJob(jobKey);
        } catch (SchedulerException e) {
            LOGGER.error("Failed to delete job by jobKey {}, because: {}", jobKey, e.getMessage(), e);
            return false;
        }
        return true;
    }

    private SimpleScheduleBuilder createSchedule(final ScheduleFrequency scheduleFrequency) {

        final int intervalInHours = switch (scheduleFrequency) {
            case BIDAILY -> 12;
            case DAILY -> 24;
            case BIWEEKLY -> (24 * 7) / 2;
            case WEEKLY -> 24 * 7;
            case MONTHLY -> 24 * 30;
            default -> 24 * 30;
        };

        return SimpleScheduleBuilder.simpleSchedule()
                .withIntervalInHours(intervalInHours)
                .repeatForever();
    }
}
