package net.agilob.untitled.quartz;

import java.time.Duration;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.UUID;

import io.quarkus.test.TestTransaction;
import net.agilob.untitled.repository.*;
import org.junit.jupiter.api.Test;
import org.quartz.JobKey;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;

import io.quarkus.test.common.QuarkusTestResource;
import io.quarkus.test.junit.QuarkusTest;
import net.agilob.untitled.PostgresResource;
import org.quartz.Trigger;

import jakarta.inject.Inject;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.assertNotNull;


@QuarkusTest
@TestTransaction
@QuarkusTestResource(PostgresResource.class)
public class JobManagerTest {

    @Inject
    RepositoryResource repositoryResource;

    @Inject
    Scheduler scheduler;

    @Test
    void testCreateJobWithStartDate() {
        final var repoDto = RepositoryUtil.createRepositoryDTO();
        repoDto.startDateTime = LocalDateTime.now().plusYears(1);

        final var jc = new JobManager();
        jc.scheduler = scheduler;

        try {
            final var triggerTime = jc.createJob(repoDto.id, repoDto);
            assertEquals(triggerTime.getYear(), repoDto.startDateTime.getYear());
        } catch (SchedulerException e) {
            fail(e.getMessage());
        }
    }

    @Test
    void testCreateJobWithoutStartDate() {
        final var repoDto = RepositoryUtil.createRepositoryDTO();

        final var jc = new JobManager();
        jc.scheduler = scheduler;

        try {
            final var triggerTime = jc.createJob(repoDto.id, repoDto);
            assertEquals(triggerTime.truncatedTo(ChronoUnit.SECONDS),
                    LocalDateTime.now().plusMinutes(1).truncatedTo(ChronoUnit.SECONDS));
        } catch (SchedulerException e) {
            fail(e.getMessage());
        }
    }

    @Test
    void quartzTriggerCreated() throws SchedulerException {
        final var repositoryDTO = new RepositoryDTO();
        repositoryDTO.name = UUID.randomUUID().toString();
        repositoryDTO.link = "https://b.agilob.net/" + UUID.randomUUID();
        repositoryDTO.savePath = "/tmp";
        repositoryDTO.repositoryType = RepositoryType.YT_DL;
        repositoryDTO.scheduleFrequency = ScheduleFrequency.DAILY;

        final RepositoryDTO savedRepo = repositoryResource.createRepository(repositoryDTO);

        assertEquals(repositoryDTO.name, savedRepo.name);
        assertEquals(repositoryDTO.link, savedRepo.link);
        assertEquals(repositoryDTO.savePath, savedRepo.savePath);
        assertNotNull(savedRepo);
        assertNotNull(savedRepo.nextFireTime);

        List<? extends Trigger> triggersOfJob = scheduler.getTriggersOfJob(new JobKey(savedRepo.name, savedRepo.repositoryType.toString()));
        assertEquals(1, triggersOfJob.size());
        final Trigger trigger = triggersOfJob.get(0);

        final var triggerTime = trigger.getNextFireTime().toInstant()
                .atZone(ZoneId.systemDefault())
                .toLocalDateTime();

        assertEquals(savedRepo.nextFireTime, triggerTime);

        final LocalDateTime in1Second = LocalDateTime.now().plus(Duration.of(1, ChronoUnit.SECONDS));
        final LocalDateTime inOver1Minute = LocalDateTime.now().plus(Duration.of(65, ChronoUnit.SECONDS));

        assertEquals(in1Second.compareTo(triggerTime), -1);
        assertEquals(inOver1Minute.compareTo(triggerTime), 1);
    }
}
