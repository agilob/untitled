package net.agilob.untitled.repository;

import java.util.UUID;
import java.util.concurrent.ThreadLocalRandom;

public class RepositoryUtil {

    public static Repository createRepository() {
        var repository = new Repository();
        repository.name = UUID.randomUUID().toString();
        repository.link = "https://b.agilob.net/" + UUID.randomUUID();
        repository.repositoryType = RepositoryType.YT_DL;
        repository.savePath = "/tmp/" + ThreadLocalRandom.current().nextInt();
        repository.scheduleFrequency = ScheduleFrequency.DAILY;
        return repository;
    }

    public static RepositoryDTO createRepositoryDTO() {
        var repository = new RepositoryDTO();
        repository.name = UUID.randomUUID().toString();
        repository.link = "https://b.agilob.net/" + UUID.randomUUID();
        repository.repositoryType = RepositoryType.YT_DL;
        repository.savePath = "/tmp/" + ThreadLocalRandom.current().nextInt();
        repository.scheduleFrequency = ScheduleFrequency.DAILY;
        return repository;
    }

}
