package net.agilob.untitled.repository;

import com.tngtech.archunit.core.domain.JavaClasses;
import com.tngtech.archunit.core.importer.ClassFileImporter;
import com.tngtech.archunit.junit.AnalyzeClasses;
import com.tngtech.archunit.junit.ArchTest;
import com.tngtech.archunit.lang.ArchRule;
import org.junit.jupiter.api.Test;

import static com.tngtech.archunit.lang.syntax.ArchRuleDefinition.methods;

@AnalyzeClasses(packages = "net.agilob.untitled.repository")
public class RepositoryArchitectureTest {

    @ArchTest
    static ArchRule publicMethodsInResourceShouldNotReturnEntity =
            methods()
                    .that()
                    .areDeclaredInClassesThat()
                    .haveSimpleNameContaining("Resource")
                    .and().arePublic()
                    .should().notHaveRawReturnType(Repository.class)
                    .because("we do not want to couple the client code directly to the return types of the encapsulated module");

    @Test
    public void abstractClassesAreAbstract() {
        final JavaClasses importedClasses = new ClassFileImporter()
                .importPackages("net.agilob.untitled.repository");
        publicMethodsInResourceShouldNotReturnEntity.check(importedClasses);
    }

}
