package net.agilob.untitled.repository;

import static io.restassured.RestAssured.given;
import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.core.IsEqual.equalTo;
import static org.hamcrest.core.IsNull.nullValue;
import static org.junit.jupiter.api.Assertions.fail;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

import jakarta.inject.Inject;

import org.apache.http.entity.ContentType;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import io.quarkus.test.common.QuarkusTestResource;
import io.quarkus.test.junit.QuarkusMock;
import io.quarkus.test.junit.QuarkusTest;
import net.agilob.untitled.PostgresResource;
import net.agilob.untitled.quartz.JobManager;

@QuarkusTest
@QuarkusTestResource(PostgresResource.class)
class RepositoryResourceTest {

    @Inject
    RepositoryRepo repositoryRepo;

    @Test
    void getByValidId() {
        final var r1 = RepositoryUtil.createRepository();
        repositoryRepo.save(r1);

        given()
            .when()
            .get("/repository/" + r1.id)
            .then()
            .assertThat()
            .statusCode(200)
            .body("id", notNullValue())
            .body("link", equalTo(r1.link))
            .body("repositoryType", equalTo(r1.repositoryType.toString()))
            .body("savePath", equalTo(r1.savePath));
    }

    @Test
    void getByFakeId() {

        given()
                .when()
                .get("/repository/fake_")
                .then()
                .assertThat()
                .statusCode(404)
                .body(is("Repository with id fake_ not found"));
    }

    @Test
    void createChannel() {
        given()
                .body("""
                        {
                             "name": "agilob_random",
                             "link": "https://b.agilob.net",
                             "savePath": "/tmp/",
                             "repositoryType": "YT_DL",
                             "scheduleFrequency": "DAILY"
                        }""".trim())
                .contentType(ContentType.APPLICATION_JSON.toString())
                .when()
                .post("/repository")
                .then()
                .assertThat()
                .statusCode(200)
                .body("id", notNullValue())
                .body("name", equalTo("agilob_random"))
                .body("repositoryType", equalTo("YT_DL"))
                .body("link", equalTo("https://b.agilob.net"))
                .body("savePath", equalTo("/tmp/"))
                .body("nextFireTime", notNullValue());
    }

    @Test
    void createRepoMissingName() {
        given()
                .body("""
                        {
                             "link": "https://b.agilob.net",
                             "savePath": "/tmp/",
                             "repositoryType": "YT_DL",
                             "scheduleFrequency": "DAILY"
                        }""".trim())
                .contentType(ContentType.APPLICATION_JSON.toString())
                .when()
                .post("/repository")
                .then()
                .assertThat()
                .statusCode(400)
                .body("exception", nullValue())
                .body("parameterViolations[0].message", equalTo("name is required"));
    }

    @Test
    void createRepoMissingLink() {
        given()
                .body("""
                        {
                             "name": "name",
                             "savePath": "/tmp/",
                             "repositoryType": "YT_DL",
                             "scheduleFrequency": "DAILY"
                        }""".trim())
                .contentType(ContentType.APPLICATION_JSON.toString())
                .when()
                .post("/repository")
                .then()
                .assertThat()
                .statusCode(400)
                .body("exception", nullValue())
                .body("parameterViolations[0].message", equalTo("link is required"));
    }

    @Test
    void createRepoMissingScheduleFrequency() {
        given()
                .body("""
                        {
                             "name": "name",
                             "link": "https://b.agilob.net",
                             "savePath": "/tmp",
                             "repositoryType": "YT_DL"
                        }""".trim())
                .contentType(ContentType.APPLICATION_JSON.toString())
                .when()
                .post("/repository")
                .then()
                .assertThat()
                .statusCode(400)
                .body("exception", nullValue())
                .body("parameterViolations[0].message", equalTo("scheduleFrequency is required"));
    }

    @Test
    void createRepoMissingSavePath() {
        given()
                .body("""
                        {
                             "name": "name",
                             "link": "https://b.agilob.net",
                             "repositoryType": "YT_DL",
                             "scheduleFrequency": "DAILY"
                        }""".trim())
                .contentType(ContentType.APPLICATION_JSON.toString())
                .when()
                .post("/repository")
                .then()
                .assertThat()
                .statusCode(400)
                .body("exception", nullValue())
                .body("parameterViolations[0].message", equalTo("savePath is required"));
    }

    @Test
    void createRepoMissingRepoType() {
        given()
                .body("""
                        {
                             "name": "name",
                             "link": "https://b.agilob.net",
                             "savePath": "/tmp",
                             "scheduleFrequency": "DAILY"
                        }""".trim())
                .contentType(ContentType.APPLICATION_JSON.toString())
                .when()
                .post("/repository")
                .then()
                .assertThat()
                .statusCode(400)
                .body("exception", nullValue())
                .body("parameterViolations[0].message", equalTo("repositoryType is required"));
    }

    @Test
    void createRepoMissingField() {
        given()
                .body("""
                        {
                             "savePath": "/tmp",
                             "repositoryType": "YT_DL"
                        }""".trim())
                .contentType(ContentType.APPLICATION_JSON.toString())
                .when()
                .post("/repository")
                .then()
                .assertThat()
                .statusCode(400)
                .body("exception", nullValue())
                .body("parameterViolations[0].message", containsString("is required"))
                .body("parameterViolations[1].message", containsString("is required"))
                .body("parameterViolations[2].message", containsString("is required"));

    }

    @Test
    void createChannelInFakeDirectory() {
        given()
                .body("""
                        {
                             "name": "agilob",
                             "link": "https://b.agilob.net",
                             "savePath": "/tmp/totallyFakeDirectory",
                             "repositoryType": "YT_DL",
                             "scheduleFrequency": "DAILY"
                        }""".trim())
                .contentType(ContentType.APPLICATION_JSON.toString())
                .when()
                .post("/repository")
                .then()
                .assertThat()
                .statusCode(200)
                .body("id", notNullValue())
                .body("name", equalTo("agilob"))
                .body("repositoryType", equalTo("YT_DL"))
                .body("link", equalTo("https://b.agilob.net"))
                .body("savePath", equalTo("/tmp/totallyFakeDirectory"))
                .body("nextFireTime", notNullValue());
    }

    @Test
    void createChannelInFile() {
        final var filePath = "/tmp/fakeFile";
        try {
            final Path path = Files.createFile(Path.of(filePath));
            path.toFile().deleteOnExit();
        } catch (IOException e) {
            fail(e.getMessage());
        }
        given()
                .body("""
                {
                     "name": "agilob",
                     "link": "https://b.agilob.net",
                     "savePath": "/tmp/fakeFile",
                     "repositoryType": "YT_DL",
                     "scheduleFrequency": "DAILY"
                }""".trim())
                .contentType(ContentType.APPLICATION_JSON.toString())
                .when()
                .post("/repository")
                .then()
                .assertThat()
                .statusCode(400)
                .body(equalTo("Save path /tmp/fakeFile must be a writable directory"));
    }

    @Test
    void deleteByValidId() {
        JobManager mock = Mockito.mock(JobManager.class);
        Mockito.doReturn(true).when(mock).deleteJob(Mockito.any());
        QuarkusMock.installMockForType(mock, JobManager.class);

        final var r1 = RepositoryUtil.createRepository();
        repositoryRepo.save(r1);

        given()
                .when()
                .delete("/repository/" + r1.id)
                .then()
                .assertThat()
                .statusCode(200)
                .body(is("true"));
    }

    @Test
    void deleteJoblessPasses() {
        JobManager mock = Mockito.mock(JobManager.class);
        Mockito.doReturn(false).when(mock).deleteJob(Mockito.any());
        QuarkusMock.installMockForType(mock, JobManager.class);

        final var r1 = RepositoryUtil.createRepository();
        repositoryRepo.save(r1);

        given()
                .when()
                .delete("/repository/" + r1.id)
                .then()
                .assertThat()
                .statusCode(200)
                .body(is("true"));
    }

    @Test
    void deleteByFakeId() {
        given()
                .when()
                .delete("/repository/fake_")
                .then()
                .assertThat()
                .statusCode(200)
                .body(is("false"));
    }
}
