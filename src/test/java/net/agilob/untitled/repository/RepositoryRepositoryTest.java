package net.agilob.untitled.repository;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;

import jakarta.inject.Inject;
import jakarta.persistence.PersistenceException;

import org.junit.jupiter.api.Test;

import io.quarkus.test.TestTransaction;
import io.quarkus.test.common.QuarkusTestResource;
import io.quarkus.test.junit.QuarkusTest;
import net.agilob.untitled.PostgresResource;

@QuarkusTest
@QuarkusTestResource(PostgresResource.class)
class RepositoryRepositoryTest {

    @Inject
    RepositoryRepo repositoryRepo;

    @Test
    @TestTransaction
    public void insertionTest() {
        var repository = RepositoryUtil.createRepository();
        repositoryRepo.save(repository);
        assertNotNull(repository.id, "Id mustn't be null");
    }

    @Test
    @TestTransaction
    public void nameIsUnique() {
        var repository = new Repository();
        repository.name = "timeline 1";
        repository.link = "https://b.agilob.net";
        repository.savePath = "/tmp/";
        repository.repositoryType = RepositoryType.YT_DL;
        repository.scheduleFrequency = ScheduleFrequency.DAILY;
        repositoryRepo.save(repository);
        assertNotNull(repository.id, "Id mustn't be null");

        var repository2 = new Repository();
        repository2.name = "timeline 1";
        repository2.link = "https://b.agilob.net";
        repository2.savePath = "/tmp/";
        repository2.repositoryType = RepositoryType.YT_DL;
        repository.scheduleFrequency = ScheduleFrequency.DAILY;
        assertThrows(PersistenceException.class, () -> repositoryRepo.save(repository2));
    }

//    @Test
//    @TestTransaction
//    public void updateUpdates() {
//        var repository = RepositoryUtil.createRepository();
//        repositoryRepo.save(repository);

//        assertNotNull(repository.id, "Id must not be null");
//        assertNull(repository.lastRun, "lastRun must be null");

//        repositoryRepo.updateLastRunDateTime(LocalDateTime.now(), repository.id);

//        var updatedRepo = repositoryRepo.findById(repository.id);
//        assertNotNull(updatedRepo.lastRun, "lastRun must not be null");
//    }

}
