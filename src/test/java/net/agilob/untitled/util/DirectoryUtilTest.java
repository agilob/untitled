package net.agilob.untitled.util;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.io.IOException;

import static org.junit.jupiter.api.Assertions.*;

class DirectoryUtilTest {

    private File tmpFile;

    @Test
    void createDirectory() {
        tmpFile = DirectoryUtil.createDirectory("/tmp/totallyNormalDirectory");
    }

    @Test
    void createDirectoryWhereAFileIs() {
        tmpFile = new File("/tmp/totallyNormalFileNothingToSeeHere");
        try {
            tmpFile.createNewFile();
        } catch (IOException e) {
            fail(e.getMessage());
        }
        final var exception = Assertions.assertThrows(MustBeDirectoryException.class, () -> {
            tmpFile = DirectoryUtil.createDirectory("/tmp/totallyNormalFileNothingToSeeHere");
        });
        assertEquals("Save path /tmp/totallyNormalFileNothingToSeeHere must be a writable directory", exception.getMessage());
    }

    @AfterEach
    public void markFileForRemoval() {
        tmpFile.delete();
    }
}
