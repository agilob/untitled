# Untitled

Untitled will help you archive online repositories from websites like YouTube to your data storage.

## Used technologies

### Quartz
[Quartz](http://www.quartz-scheduler.org/) for scheduling jobs, so you can run it on multiple servers and never miss a schedule.

### yt-dlp
[yt-dlp](https://github.com/yt-dlp/yt-dlp) gives Untitled powers to backup [hundreds of websites](https://github.com/yt-dlp/yt-dlp/blob/master/supportedsites.md).


# Deployment

## Compose
Example docker/podman compose file to run Untitled locally
```yaml
version: "3.3"

services:

  postgres-untitled:
    image: postgres:15.1-alpine
    container_name: postgres-untitled
    hostname: postgres-untitled
    networks:
      - untitled-network
    environment:
      - POSTGRES_USER=untitled
      - POSTGRES_PASSWORD=untitled
      - POSTGRES_DB=untitled
    expose:
      - 5432

  untitled:
    image: registry.gitlab.com/agilob/untitled:latest
    container_name: untitled
    hostname: untitled
    networks:
      - untitled-network
    environment:
      - QUARKUS_DATASOURCE_USERNAME=untitled
      - QUARKUS_DATASOURCE_PASSWORD=untitled
      - QUARKUS_DATASOURCE_JDBC_URL=jdbc:postgresql://untitled-network:5432/untitled
      - QUARKUS_HTTP_PORT=8080
      # - QUARKUS_LOG_LEVEL=DEBUG
    volumes:
      - /tmp/change_me:/untitled
    ports:
      - 8080:8080
    restart: unless-stopped

networks:
  untitled-network:
    name: untitled-network

```

## Kubernetes
Example Kubernetes deployment to run Untitled in a cluster, saving data to an NFS share
```yaml
apiVersion: apps/v1
kind: Deployment
metadata:
  name: untitled
  labels:
    app: untitled
spec:
  selector:
    matchLabels:
      app: untitled
  replicas: 1
  strategy:
    type: RollingUpdate
    rollingUpdate:
      maxUnavailable: 1
      maxSurge: 1
  template:
    metadata:
      labels:
        app: untitled
    spec:
      volumes:
        - name: untitled-storage
          nfs:
            # NSF share is expected to have /untitled/downloads directory, you can change this
            server: nsfshare.lan
            path: /untitled/downloads
      nodeSelector:
        kubernetes.io/arch: amd64
      hostNetwork: true
      # Should be the same number as quarkus.shutdown.timeout in application.properties, default 60
      # Can be set as env QUARKUS_SHUTDOWN_TIMEOUT
      terminationGracePeriodSeconds: 60
      containers:
        - name: untitled
          imagePullPolicy: Always
          image: 'registry.gitlab.com/agilob/untitled:latest'
          env:
            - name: QUARKUS_DATASOURCE_USERNAME
              value: 'untitled'
            - name: QUARKUS_DATASOURCE_PASSWORD
              value: 'untitled'
            - name: QUARKUS_DATASOURCE_JDBC_URL
              value: jdbc:postgresql://postgres.database:5432/untitled
          ports:
            - containerPort: 8080
          volumeMounts:
            # The storage will be mounter to /yt-dlp, use this path in webui
            - mountPath: /untitled
              name: untitled-storage
          resources:
            requests:
              memory: '128Mi'
              cpu: '1'
            limits:
              memory: '512Mi'
              cpu: '2'
```

## Configuration
### yt-dlp
You can provide `yt-dlp` with a configuration file. Location of the file will be automatically added to all yt-dlp processes, if environment variable is configured.

If the environment variable is configured, but file does not exist or is not readable you will see error messages in logs.

Set the environment variable to an absolute path where yt-dlp compatible config file lives. Following practice from above `deployment.yaml` it would be:

```yaml
- name: YTDLP_CONFIG
  value: /untitled/yt-dlp.conf
```